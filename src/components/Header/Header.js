import React from "react";
import { Link } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Container,
  Box,
  InputBase,
  Badge,
  IconButton,
  Typography,
  Divider,
  Drawer,
} from "@mui/material";
import { styled } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";

import logo from "../../assets/images/logo.jpg";

import LoginIcon from "@mui/icons-material/Login";
import FavoriteIcon from "@mui/icons-material/Favorite";
import MenuIcon from "@mui/icons-material/Menu";

import "./Header.css";
import {
  changePage,
  changeSearchGameValue,
  fetchGames,
} from "../../store/slices/gameSlice";

const StyledTolBar = styled(Toolbar)({
  display: "flex",
  width: "100%",
  justifyContent: "space-between",
});

const SearchHeader = styled("div")(({ theme }) => ({
  backgroundColor: "white",
  padding: "8px 12px",
  borderRadius: theme.shape.borderRadius,
  width: "50%",
}));

const Icons = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "end",
  width: "30%",
}));

export default function Header() {
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const page = useSelector((state) => state.games.page);
  const dispatch = useDispatch();

  const handleChangePage = () => {
    dispatch(fetchGames(page));

    dispatch(changePage(1));
  };

  const onChangeSearchGameValue = (value) => {
    dispatch(changeSearchGameValue(value));
    console.log(value);
  };

  return (
    <AppBar className="header" position="sticky" color="darkCustom">
      <StyledTolBar disableGutters className="header_offset">
        <Box className="logo" sx={{ mr: 5 }} onClick={() => handleChangePage()}>
          <Link to="/">
            <img src={logo} className="logo" />
          </Link>
        </Box>
        <SearchHeader sx={{ display: { md: "none", xs: "none", lg: "flex" } }}>
          <InputBase
            style={{ width: "100%" }}
            placeholder="Поиск...."
            onChange={(e) => onChangeSearchGameValue(e.target.value)}
          />
        </SearchHeader>
        <Icons sx={{ display: { md: "none", xs: "none", lg: "flex" } }}>
          <Link className="btn_header" to="/favorite">
            <Badge color="error">
              <FavoriteIcon />
            </Badge>
          </Link>
          <Link className="btn_header">
            <LoginIcon />
          </Link>
        </Icons>
        <IconButton
          color="white"
          aria-label="open drawer"
          edge="end"
          sx={{ ml: 5, display: { lg: "none" } }}
          onClick={handleDrawerToggle}
        >
          <MenuIcon fontSize="large" sx={{ color: "white" }} />
        </IconButton>
      </StyledTolBar>

      <Drawer
        anchor="left"
        open={mobileOpen}
        onClose={() => handleDrawerToggle()}
      >
        <Box
          p={2}
          sx={{ display: "flex", flexDirection: "column" }}
          role="presentation"
          width="260px"
          textAlign="center"
        >
          <Typography variant="h4" sx={{ marginBottom: "8px" }}>
            Menu
          </Typography>
          <Icons sx={{ flexDirection: "column", width: "100%" }}>
            <Badge
              className="btn_header"
              sx={{ width: "80%", marginBottom: "8px" }}
            >
              <LoginIcon />
            </Badge>
            <Badge
              className="btn_header"
              color="error"
              sx={{ width: "80%", marginBottom: "8px" }}
            >
              <Link className="btn_header">
                <FavoriteIcon />
              </Link>
            </Badge>
          </Icons>
        </Box>
      </Drawer>
    </AppBar>
  );
}
