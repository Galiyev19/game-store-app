import React, {useEffect, useState} from 'react'
import {Box} from '@mui/material';
import {Link} from 'react-router-dom'


const ListMenuDashBoard = ({menu}) => {
    const [open, setOpen] = React.useState(false);

    const [platforms,setPlatforms] = useState(null);
    const [genres,setGenres] = useState(null)

    const handleClick = () => {
        setOpen(!open)
    };



   const getGenres = async () => {
       const response = await  fetch("https://api.rawg.io/api/genres?key=2c1a97409a5d4e84a9ce0d6858e1bd1a")
       const result  = await response.json()
       setGenres(result.results)
   }

   useEffect(() => {

       getGenres()
   },[])
    
    return(
        <Link to={menu.link} className="list_item_menu" onClick={() => handleClick()}>
            <Box sx={{display: "flex", alignItems: "center", width: "100%"}}>
            <span className="icon_box">
                {menu.icon}
            </span>
            <span className="menu_text">{menu.menu_name}</span>
            </Box>
        </Link>

    )
}

export  default ListMenuDashBoard;