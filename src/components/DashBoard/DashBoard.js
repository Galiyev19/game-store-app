import React, {useEffect, useState} from "react";
import {Link} from 'react-router-dom'

import HomeIcon from '@mui/icons-material/Home';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import CategoryIcon from '@mui/icons-material/Category';
import {Skeleton, Stack} from "@mui/material";

import ListMenuDashBoard from "./ListMenuDashBoard/ListMenuDashBoard";

import "./DashBoard.css"

const menu_list = [
    {
        id: 1,
        menu_name: "Platforms",
        icon: <SportsEsportsIcon/>,
        open: false,
        link: "/platforms"
    },
    {
        id: 2,
        menu_name: "Genres",
        icon: <CategoryIcon/>,
        open: false,
        link: "/genres"
    }
]

const DashBoard = () => {

    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(true);
        }, 1500);
    }, []);

    return(
        <nav className="dashBoard">
            {
                isLoading ?
            <div className="menu_dashBoard">
                <Link to="/" className="list_item_menu">
                     <span className="icon_box"><HomeIcon/></span>
                    <span className="menu_text">Home</span>
                </Link>
                <ul className='list_menu'>
                    {
                        menu_list.map(menu =>  <ListMenuDashBoard key={menu.id} menu={menu}/>)
                    }
                </ul>
            </div>
                    : <Stack>
                        <Skeleton  variant="rectangular"
                                   width={300}
                                   height={40}
                                   sx={{ bgcolor: "gray", gap: "30px" }}
                                   animation="pulse"/>
                    </Stack>
            }
        </nav>
    )
}

export  default  DashBoard;