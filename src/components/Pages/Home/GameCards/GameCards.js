import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { Grid } from "@mui/material";

import { changeName } from "../../../../store/slices/gameDetailSlice";
import CardItem from "./GameCardItem/CardItem";

export default function GameCards({games}) {
  // const games = useSelector((state) => state.games.games);
  const searchGame = useSelector((state) =>
    state.games.searchGame.toLowerCase()
  );

  const dispatch = useDispatch();
  const handleChangeName = (name) => {
    dispatch(changeName(name));
  };

  return (
    <Grid container spacing={4}>
      <CardItem
        games={games}
        searchGame={searchGame}
        handleChangeName={handleChangeName}
      />
    </Grid>
  );
}
