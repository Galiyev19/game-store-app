import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import {
  Grid,
  Card,
  CardMedia,
  CardActionArea,
  CardContent,
  Typography,
  Stack,
  Skeleton,
  CardActions,
    Box
} from "@mui/material";

import { styled } from "@mui/system";


const StyledCard = styled(Card)(({ theme }) => ({
  width: 200,
  height: 200,
  borderRadius: "18px",
  transition: 'transform 0.3s ease',
  '&:hover': {
    transform: 'scale(1.1)',
  },
}));


export default function CardItem({ games, searchGame, handleChangeName }) {
  const [isLoading, setIsLoading] = useState(false);

  const filterGames = games.filter((item) => {
    return item.name.toLowerCase().includes(searchGame);
  });

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(true);
    }, 1000);
  }, []);
  return (
    <>
      {!isLoading
        ? filterGames.map((item) => (
            <Grid item key={item.id}  lg={3} md={6} xs={14}>
              <Stack>
                <Skeleton
                  variant="rectangular"
                  width={300}
                  height={400}
                  sx={{ bgcolor: "gray", gap: "30px" }}
                  animation="pulse"
                />
              </Stack>
            </Grid>
          ))
        : filterGames.map((game) => (
            <Grid item key={game.id}  lg={3} md={6} xs={14}>
              <StyledCard
                as={Link}
                to={`game/${game.id}`}
                onClick={() => handleChangeName(game.id)}
              >
                <CardActionArea>
                  <CardMedia
                    component="img"
                    image={game.background_image}
                    className="card_img"
                  />
                  <CardContent sx={{ backgroundColor: "black" }}>
                    <Typography color="white">{game.name}</Typography>
                    <Box sx={{display: 'flex', justifyContent: "space-between", alignItems: "center"}}>
                      <Box>
                    {game.parent_platforms.map((item) => (
                      <img
                        key={item.platform.id}
                        className="img_platform"
                        src={require(`../.././../../../assets/images/platforms/${item.platform.slug}.svg`)}
                        alt={item.platform.slug}
                      />
                    ))}
                      </Box>
                    <Box sx={{border: 1, padding: "4px", borderRadius: "4px", borderColor: "green"}}>
                      <Typography sx={{color: "green", fontWeight: "bold"}}>
                        {game.metacritic}
                      </Typography>
                    </Box>
                    </Box>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                </CardActions>
              </StyledCard>
            </Grid>
          ))}
    </>
  );
}
