import React, { useEffect, useState } from "react";
import {
  Container,
  FormControl,
  Select,
  InputLabel,
  MenuItem
} from "@mui/material";


import GameCards from "./GameCards/GameCards";


import "./Home.css";

const date = [{id: 1, date: "1970-1979"},{id: 2, date: "1980-1989"},{id: 3, date: "1990-1999"},{id: 4, date: "2000-2009"},{id: 5, date: "2010-2020"},{id: 6, date: "2010-2023"}]



export default function Home() {
  // const games = useSelector((state) => state.games.games);
  // const page = useSelector((state) => state.games.page);

  // const [page, setPage] = useState(1);




  const [games,setGames] = useState([])
  let page = 1


  const loadMoreGame =  async () => {
    try{
      const response = await fetch(`https://api.rawg.io/api/games?dates=1970-09-01%2C2023-12-31&key=2c1a97409a5d4e84a9ce0d6858e1bd1a&page=${page}`)
      const data  = await response.json()
      const games = []
      data.results.forEach((g) => games.push(g) )
      setGames((oldData) => [...oldData,...games])
      page += 1
    }catch (error) {
      return error
    }

  }

  const handleScroll = (event) => {
    if(
        window.innerHeight + event.target.documentElement.scrollTop + 1 >
        event.target.documentElement.scrollHeight
    ){
      loadMoreGame()
    }
  }


  useEffect(() => {
    loadMoreGame()
    // dispatch(fetchGames(page));
    window.addEventListener('scroll',handleScroll)
    // console.log(games)
  }, []);

  return (
    <main className="game_card_grid">
      <Container maxWidth="xl" className='container'>
        <GameCards games={games} />
      </Container>
    </main>
  );
}
