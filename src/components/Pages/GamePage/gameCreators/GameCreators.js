import React, {useEffect, useState} from 'react'
import { Card, Box, CardMedia, CardContent, Typography } from "@mui/material"
import {styled} from "@mui/system"
import {Swiper,SwiperSlide} from 'swiper/react'
import { EffectCoverflow, Pagination } from "swiper";
import PersonAddIcon from '@mui/icons-material/PersonAdd';

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "./GameCreators.css"

const StyledBox = styled("div")({
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    minHeight: "100%",
    padding: "32px 24px"
})






const GameCreators = ({ gameName }) => {

    const [creators,setCreators] = useState([])
    const getCreators =  async () => {
        try{
            const response  = await fetch(`https://api.rawg.io/api/games/${gameName}/development-team?&key=2c1a97409a5d4e84a9ce0d6858e1bd1a`)
            const data =  await response.json()
            setCreators(data.results)
        }catch (e) {
            return e
        }
    }

    useEffect(() => {
        console.log(creators)
        getCreators();
    },[gameName])

    return(
        <Swiper
            effect={"coverflow"}
            grabCursor={true}
            centeredSlides={true}
            slidesPerView={"auto"}
            coverflowEffect={{
                rotate: 20,
                stretch: 0,
                depth: 200,
                modifier: 1,
                slideShadows: true,
            }}
            pagination={true}
            modules={[EffectCoverflow, Pagination]}>
            {
                creators.map(item =>
                    <SwiperSlide  style={{backgroundImage: `linear-gradient(rgba(32, 32, 32, 0.5), rgb(32, 32, 32) 95%),url(${item.image_background})`}} key={item.id}>
                        <StyledBox>
                            <Box sx={{display: "flex", flexDirection: "column",textAlign: "center", alignItems: "center",justifyContent: "center"}}>
                            {item.image ? <img src={item.image} alt={item.name}/> : null}
                            <Typography sx={{color: "#fff", fontWeight: "bold", fontSize: "24px"}}>{item?.name}</Typography>
                            {
                                item.positions.map(pos =>
                                    <Typography key={pos.id} sx={{color: "#fff",fontSize: "16px",marginRight: "4px",textDecoration: "underline"}}>{pos?.name} </Typography>)
                            }
                            </Box>
                            <Box sx={{width: "100%",display: 'flex', alignItems: "flex-start", flexDirection: "column"}}>
                                <Typography sx={{color: "#fff"}}>Games</Typography>
                                {
                                    item.games.map(game => <Box key={game.id} sx={{display:"flex",width: "100%", justifyContent: "space-between"}}>
                                        <Typography  sx={{color: "#f3f5f5",flexWrap: 'wrap'}}>{game.name}</Typography>
                                        <Typography sx={{color: "#fff", alignItems: 'center',}}>{game.added} <PersonAddIcon sx={{fontSize: "18px",alignItems: 'center'}}/></Typography>
                                    </Box>)
                                }
                            </Box>
                        </StyledBox>
                    </SwiperSlide>
                )
            }
        </Swiper>
    )
}

export  default  GameCreators;