import React, { useRef } from "react";
import KeyboardDoubleArrowLeftSharpIcon from "@mui/icons-material/KeyboardDoubleArrowLeftSharp";
import KeyboardDoubleArrowRightSharpIcon from "@mui/icons-material/KeyboardDoubleArrowRightSharp";

import "./SliderWrapper.css";

export default function SliderWrapper({ images, screenIndex, setScreenIndex }) {
  const ref = useRef(null);

  const changeIndex = (index) => {
    setScreenIndex(index);
  };

  const nextSlide = () => {
    setScreenIndex(screenIndex === images.length - 1 ? 0 : screenIndex + 1);
    // ref.current.scrollLeft += 160;

    screenIndex === images.length - 1
      ? ref.current.scrollTo(0, 0)
      : (ref.current.scrollLeft += 160);
  };

  const prevSlide = () => {
    setScreenIndex(screenIndex === 0 ? images.length - 1 : screenIndex - 1);
  };

  return (
    <div className="slider_wrapper">
      <button className="slider_wrapper_btn" onClick={() => prevSlide()}>
        <KeyboardDoubleArrowLeftSharpIcon fontSize="small" />
      </button>
      <div className="slider_wrapper_item" ref={ref}>
        {images.map((item, index) => (
          <div key={item.id} className="slider_item">
            <img
              src={item.image}
              onClick={() => changeIndex(index)}
              className={
                index === screenIndex
                  ? "slider_wrapper_img active"
                  : "slider_wrapper_img"
              }
            />
          </div>
        ))}
      </div>
      <button className="slider_wrapper_btn" onClick={() => nextSlide()}>
        <KeyboardDoubleArrowRightSharpIcon fontSize="small" />
      </button>
    </div>
  );
}
