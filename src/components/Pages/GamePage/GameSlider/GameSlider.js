import React, { useState } from "react";
import KeyboardDoubleArrowLeftSharpIcon from "@mui/icons-material/KeyboardDoubleArrowLeftSharp";
import KeyboardDoubleArrowRightSharpIcon from "@mui/icons-material/KeyboardDoubleArrowRightSharp";



import "./GameSlider.css";

export default function GameSlider({ images, screenIndex, setScreenIndex }) {
  const nextSlide = () => {
    setScreenIndex(screenIndex === images.length - 1 ? 0 : screenIndex + 1);
  };

  // console.log(slideIndex);

  const prevSlide = () => {
    setScreenIndex(screenIndex === 0 ? images.length - 1 : screenIndex - 1);
  };
  return (
    <div className="container_slider">
      {images.map((item, index) => (
        <div
          key={item.id}
          className={index === screenIndex ? "slide active-anim" : "slide"}
        >
          {index === screenIndex && <img src={item.image} />}
        </div>
      ))}
      <button className="btn_slide next" onClick={() => nextSlide()}>
        <KeyboardDoubleArrowRightSharpIcon fontSize="large" />
      </button>
      <button className="btn_slide prev" onClick={() => prevSlide()}>
        <KeyboardDoubleArrowLeftSharpIcon fontSize="large" />
      </button>
    </div>
  );
}
