import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import "./GameSideInfo.css";

export default function GameSideInfo() {
  const gameDetail = useSelector((state) => state.gameDetail.gameDetail);

  const [developers, setDevelopers] = useState(null);
  //   console.log(gameDetail.developers[0]);

  useEffect(() => {
    setDevelopers(gameDetail.developers);
    // console.log(gameDetail.developers);
  }, []);

  return (
    <>
      <h2 className="game_side_title">{gameDetail.name}</h2>

      <div className="game_side_description">
        <span className="game_side_description_text text_gray">
          {gameDetail?.developers?.length > 1 ? "Developers" : "Developer"}
        </span>
        <div className="game_side_description_item">
          {gameDetail.developers?.map((item) => (
            <span
              key={item.id}
              className="game_side_description_text text_white"
            >
              {item.name}
            </span>
          ))}
        </div>
      </div>
      <div className="game_side_description">
        <span className="game_side_description_text text_gray">
          Release Date
        </span>
        <span className="game_side_description_text text_white">
          {gameDetail.released}
        </span>
      </div>
      <div className="game_side_description">
        <span className="game_side_description_text text_gray">Website</span>
        <a
          href={gameDetail.website}
          target="_blank"
          className="game_side_description_text text_white"
        >
          {gameDetail.website}
        </a>
      </div>
      <div className="game_side_description">
        <span className="game_side_description_text text_gray">Publishers</span>
        <div className="game_side_description_item">
          {gameDetail.publishers?.map((item) => (
            <span
              key={item.id}
              className="game_side_description_text text_white"
            >
              {item.name}
            </span>
          ))}
        </div>
      </div>
    </>
  );
}
