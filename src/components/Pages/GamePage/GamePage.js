import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchGameDetail } from "../../../store/slices/gameDetailSlice";
import { useNavigate, useParams } from "react-router-dom";

import ArrowBackIosNewSharpIcon from "@mui/icons-material/ArrowBackIosNewSharp";
import VerticalAlignBottomSharpIcon from "@mui/icons-material/VerticalAlignBottomSharp";

import RatingGame from "./RatingGame/RatingGame";
import GameSlider from "./GameSlider/GameSlider";
import SliderWrapper from "./GameSlider/SliderWrapper/SliderWrapper";
import GameSideInfo from "./GameSideInfo/GameSideInfo";
import GameCreators from "./gameCreators/GameCreators";

import "./GamePage.css";
import { Skeleton, Stack, Box, Typography } from "@mui/material";

const GamePage = () => {
  const name = useSelector((state) => state.gameDetail.name);
  const gameDetail = useSelector((state) => state.gameDetail.gameDetail);

  const [gameScreen, SetGameScreen] = useState([]);

  const [screenIndex, setScreenIndex] = useState(0);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const gameId = useParams();

  const getVideo = async () => {
    const response = await fetch(
      `https://api.rawg.io/api/games/${gameId.id}/movies?&key=2c1a97409a5d4e84a9ce0d6858e1bd1a`
    );

    const result = await response.json();

    console.log(result.results);
    return result;
  };

  const [isLoading, setIsLoading] = useState(false);

  const getScreen = async () => {
    try {
      const response = await fetch(
        `https://api.rawg.io/api/games/${gameId.id}/screenshots?&key=2c1a97409a5d4e84a9ce0d6858e1bd1a&`
      );

      const result = await response.json();
      SetGameScreen(result.results);

    } catch (e) {}
  };

  useEffect(() => {
    dispatch(fetchGameDetail(parseInt(gameId.id)));
    // console.log(gameDetail);
    getVideo();
    getScreen();
    window.scrollTo(0, 0);
  }, [name]);

  return (
    <div className="game_detail_main">
      {/* BUTTON GO BACK */}
      <button className="back_button" onClick={() => navigate(-1)}>
        <ArrowBackIosNewSharpIcon />
        Back
      </button>
      {/* GAME HEADER */}
      {!isLoading ? (
        <div className="game_detail_header">
          <h1 className="game_header_title">{gameDetail.name}</h1>
          <RatingGame rating={gameDetail.rating} />
        </div>
      ) : (
        <Stack sx={{ marginBottom: "50px" }}>
          <Skeleton
            variant="text"
            sx={{
              bgcolor: "gray",
              fontSize: "2rem",
              marginBottom: "33.5px",
              borderRadius: "12px",
            }}
            animation="pulse"
          />
          <Skeleton
            variant="rectangular"
            width={210}
            height={60}
            sx={{ bgcolor: "gray" }}
            animation="pulse"
          />
        </Stack>
      )}

      {/* GAME BANNER SLIDER SCREEN */}
      {!isLoading ? (
        <div className="game_banner">
          <div className="game_screenshots">
            <GameSlider
              images={gameScreen}
              setScreenIndex={setScreenIndex}
              screenIndex={screenIndex}
            />
          </div>
          <div className="game_side_info">
            <GameSideInfo gameDetail={gameDetail} />
          </div>
        </div>
      ) : (
        <Skeleton
          variant="rectangular"
          sx={{ width: "100%", height: "500px", bgcolor: "gray" }}
          animation="pulse"
        />
      )}
      {!isLoading ? (
        <SliderWrapper
          images={gameScreen}
          screenIndex={screenIndex}
          setScreenIndex={setScreenIndex}
        />
      ) : (
        <Skeleton
          variant="rectangular"
          sx={{ bgcolor: "gray", width: "65%", marginTop: "20px" }}
          animation="pulse"
          height={80}
        />
      )}

      {/* ABOUT GAME */}
      {!isLoading ? (
          <div className="about_game">
            <h2 className="game_side_title">About</h2>
            <p className="about_game_description">{gameDetail.description_raw}</p>
          </div>
      ) : (
          <Stack
              variant="rectangular"
              animation="pulse"
              sx={{
                bgcolor: "gray",
                width: "100%",
                height: "250",
                padding: "12px",
              }}
          >
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map((item) => (
                <Skeleton
                    key={Math.random()}
                    variant="text"
                    sx={{ fontSize: "30px", lineHeight: "32px" }}
                    animation="pulse"
                />
            ))}
          </Stack>
      )}

      {/* GAME GENRES */}
      <div className="game_genres_stores">
        <div className="game_genres">
          <h3 className="text_gray">Genres</h3>
          <div className="game_genres_text">
            {gameDetail?.genres?.map((item) => (
              <span key={item.id} className="game_genres_name_text">
                {item.name}
              </span>
            ))}
          </div>
        </div>
        {/* GAME STORES */}
        <div className="game_stores">
          <h3 className="text_gray">Stores</h3>
          <div className="game_genres_text">
            {gameDetail?.stores?.map((item) => (
              <a
                href={`https://${item.store.domain}`}
                target="_blank"
                key={item.id}
                className="game_genres_name_text"
              >
                {item.store.name}
              </a>
            ))}
          </div>
        </div>
        <div className="game_tags">
          <h3 className="text_gray">Tags</h3>
          <div className="game_genres_text">
            {gameDetail?.tags?.map((item) => (
                <span key={item.id} className="game_genres_name_text">
                {item.name},
              </span>
            ))}
          </div>
        </div>
      </div>
      <Box sx={{display: "flex", flexDirection: "column", width: "100%"}}>
        <h3 className="text_gray">Platforms</h3>
        <div className="game_genres_text">
          {gameDetail?.platforms?.map((item) => (
              <span key={item.platform.id} className="game_genres_name_text">
                {item.platform.name},
              </span>
          ))}
        </div>
        <Box sx={{display: "flex", flexDirection: "column", width: "100%"}}>
          <h3 className="text_gray">Age rating</h3>
          <span className="game_genres_name_text">{gameDetail?.esrb_rating?.name}</span>
        </Box>
      </Box>
      <div className="under_line"></div>



      <Box sx={{display: "flex", flexDirection: "column",width: "100%"}}>
        <h3 className="game_header_title">{gameDetail?.name} created by</h3>
        <GameCreators gameName={gameDetail?.slug}/>
      </Box>

      {gameDetail.reddit_description ? (
        <div className="reddit_link">
          <h3 className="game_side_title">Reddit Info</h3>
          <div className="click_info">
            <span>MORE INFO CLICK</span>
            <VerticalAlignBottomSharpIcon fontSize="small" color="#" />
          </div>
          <a
            href={gameDetail.reddit_url}
            target="_blank"
            className="reddit_link_text"
          >
            {gameDetail.reddit_description}
          </a>
        </div>
      ) : null}
    </div>
  );
};

export default GamePage;
