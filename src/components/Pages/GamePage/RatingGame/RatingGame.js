import React from 'react'
import StarIcon from '@mui/icons-material/Star';
import StarHalfIcon from '@mui/icons-material/StarHalf';
import StarBorderIcon from '@mui/icons-material/StarBorder';

import './RatingGame.css'

export default function RatingGame({rating}) {
    
    const starRatings  = Array.from({length: 5}, (elem, index) =>  {
        let number = index + 0.5
        return(
            <span key={index}>
                {
                    rating >  index + 1  
                    ? <StarIcon className='star_rating_icon'/>
                    : rating > number
                    ? <StarHalfIcon className='star_rating_icon'/>
                    : <StarBorderIcon className='star_rating_icon'/>
                }
            </span>
        )
    })

    return (
        <div className='game_rating'>
            {starRatings}
            <div className='rating_number'>
                <span className='rating_number_text'>{rating}</span>
            </div>
        </div>
    )
}
