import React, {useEffect, useState} from 'react'
import {Box, Container, Grid} from "@mui/material";
import {Link} from "react-router-dom";


const Genres = () => {

    const [genres,setGenres] = useState(null)

    const getGenres = async () => {
        const response = await fetch("https://api.rawg.io/api/genres?key=2c1a97409a5d4e84a9ce0d6858e1bd1a")
        const result = await  response.json()
        // const platforms = []
        // result.results.map(item => item.platforms.map(item => platforms.push(item)))
        // console.log(platforms)
        // setPlatform(platforms)
        setGenres(result.results)
    }




    useEffect(() => {
        getGenres()
    },[])


    return(
        <Container maxWidth="xl">
            <h1 className="game_header_title">Genres</h1>
            <Grid container spacing={4}>
                {
                    genres?.map(item => <Grid item key={item.id} xs={12} sm={12} md={6} lg={4}>>
                        <Box style={{backgroundImage: `linear-gradient(rgba(32, 32, 32, 0.5), rgb(32, 32, 32) 95%),url(${item.image_background}) `}}  className="card_platform">
                            <Link className="card_title">{item.name}</Link>
                            <Box sx={{display: "flex", width: "100%", justifyContent: "space-between", marginTop: "15%"}}>
                                <span className="card_text">Games count</span>
                                <span className="card_text">{item.games_count}</span>
                            </Box>
                            <div className="under_line"></div>
                            { item.games?.map(game =>
                                <Box as={Link} to={`/game/${game.id}`} key={game.id} sx={{display: 'flex',alignItems: "start", justifyContent: "space-between"}}>
                                    <span className="cart__item">{game.name}</span>
                                    <span className="card_text">{game.added}</span>
                                </Box>)
                            }
                        </Box>
                    </Grid>)
                }
            </Grid>

        </Container>
    )
}

export default  Genres;