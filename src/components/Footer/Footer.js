import React from "react";

import { Container } from "@mui/material";

import logo from "../../assets/images/logo.jpg";

import "./Footer.css";

export default function Footer() {
  return (
    <footer className="footer">
      <img src={logo} className="logo" />
    </footer>
  );
}
