import { createTheme } from "@mui/material/styles";


const theme = createTheme({
    palette:{
        darkCustom: {
            main: "#191919"
        }
    }
})

export default theme;