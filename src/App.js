import React from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Home from "./components/Pages/Home/Home";
import Favorite from "./components/Pages/Favorite/Favorite";
import GamePage from "./components/Pages/GamePage/GamePage";
import Platforms from "./components/Pages/Platforms/Platforms";
import DashBoard from "./components/DashBoard/DashBoard";
import Genres from "./components/Pages/Genres/Genres";


import "./App.css";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <div className="offset">
          <DashBoard/>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/favorite" element={<Favorite />} />
            <Route path="/game/:id" element={<GamePage />} />
            <Route path="/platforms" element={<Platforms/>}/>
            <Route path="/genres" element={<Genres/>}/>
          </Routes>
        </div>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
