import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import store from './store/store'
import { ThemeProvider } from "@mui/material/styles"
import { Provider } from 'react-redux';
import { fetchGames } from './store/slices/gameSlice';
import theme from './components/Theme/Theme';


const root = ReactDOM.createRoot(document.getElementById('root'));

store.dispatch(fetchGames(1))



root.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>
);


