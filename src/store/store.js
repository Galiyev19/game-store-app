import {configureStore} from '@reduxjs/toolkit'
import gameSlice from './slices/gameSlice'
import gameDetailSlice from './slices/gameDetailSlice';

const store = configureStore({
    reducer: {
        games: gameSlice,
        gameDetail: gameDetailSlice
    },
})

export default store;

