import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";


export const fetchGameDetail = createAsyncThunk('/games/fetchGameDetail', async (id) => {

    try {

        const response = await fetch(`https://api.rawg.io/api/games/${id}?&key=2c1a97409a5d4e84a9ce0d6858e1bd1a`)

        const data = await response.json();
    

        return data

    } catch (error) {
        console.log(error)
    }
});


const initialState = {
    isLoading: false,
    error: "Error",
    gameDetail: {},
    name: "",
};


const gameDetailSlice = createSlice({
    name: 'game-detail',
    initialState,
    reducers: {
        changeName: (state, action) => {
            state.name = action.payload
        }
    },
    extraReducers: (builder) => {
        builder
        .addCase(fetchGameDetail.pending, (state,action) => {
            state.isLoading = true
        })
        .addCase(fetchGameDetail.fulfilled, (state, action) => {
            state.isLoading = false
            state.gameDetail = action.payload
        })
        .addCase(fetchGameDetail.rejected, (state,action) => {
            state.error = action.error.message;
        })

    }

});



export const { changeName } = gameDetailSlice.actions;
export default gameDetailSlice.reducer;