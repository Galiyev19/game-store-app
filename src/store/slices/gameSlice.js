import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchGames = createAsyncThunk(
  "/games/fetchGames",
  async (page = 1) => {
    try {
      const response = await fetch(
        `https://api.rawg.io/api/games?dates=1970-09-01%2C2023-01-01&key=2c1a97409a5d4e84a9ce0d6858e1bd1a&page=${page}`
      );


      const data = await response.json();
      const result = [...data.results]
      return result;
    } catch (error) {
      console.log(error);
    }
  }
);

const initialState = {
  isLoading: false,
  error: "Error",
  games: [],
  page: 1,
  searchGame: "",
};

const gameSlice = createSlice({
  name: "games",
  initialState,
  reducers: {
    changePage: (state, action) => {
      state.page = action.payload;
    },
    changeSearchGameValue: (state, action) => {
      state.searchGame = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchGames.pending, (state, action) => {
        state.isLoading = true;
      })
      .addCase(fetchGames.fulfilled, (state, action) => {
        state.isLoading = false;
        state.games = action.payload;
      })
      .addCase(fetchGames.rejected, (state, action) => {
        state.error = action.error.message;
      });
  },
});

export const { changePage, changeSearchGameValue } = gameSlice.actions;
export default gameSlice.reducer;
